ELMC=elm make
ELM_FLAGS=--optimize

UGLIFY=uglifyjs
UGLIFY_FLAGS_1=--compress "pure_funcs=[F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9],pure_getters,keep_fargs=false,unsafe_comps,unsafe"
UGLIFY_FLAGS_2=--mangle

IN_INDEX=html/index.html
IN_MAIN=src/Main.elm
IN_SRCS=$(shell find src/ -type f)
IN_CSS_DIR=css
IN_CSS=$(wildcard $(IN_CSS_DIR)/*.css)
IN_IMG_DIR=imgs
IN_IMG=$(shell find $(IN_IMG_DIR)/ -type f)
IN_REDIRECTS=_redirects

BUILD_DIR=build
OUT_DIR=public
OUT_CSS_DIR=$(OUT_DIR)/css
OUT_IMG_DIR=$(OUT_DIR)/imgs

OUT_INDEX=$(OUT_DIR)/index.html
OUT_MAIN=$(OUT_DIR)/js/main.js
OUT_CSS=$(patsubst $(IN_CSS_DIR)/%.css,$(OUT_CSS_DIR)/%.css,$(IN_CSS))
OUT_IMG=$(patsubst $(IN_IMG_DIR)/%,$(OUT_IMG_DIR)/%,$(IN_IMG))
OUT_REDIRECTS=$(OUT_DIR)/_redirects

.phony: all clean

all: $(OUT_INDEX) $(OUT_MAIN) $(OUT_CSS) $(OUT_IMG) $(OUT_REDIRECTS)

clean:
	rm -rf $(OUT_DIR)

$(OUT_INDEX): $(IN_INDEX)
	@mkdir -p $(@D)
	cp $< $@

$(OUT_MAIN): $(IN_SRCS)
	@mkdir -p $(@D)
	$(ELMC) $(ELM_FLAGS) $(IN_MAIN) --output=$(BUILD_DIR)/tmp.js
	$(UGLIFY) $(BUILD_DIR)/tmp.js $(UGLIFY_FLAGS_1) | $(UGLIFY) $(UGLIFY_FLAGS_2) --output $@

$(OUT_REDIRECTS): $(IN_REDIRECTS)
	@mkdir -p $(@D)
	cp $< $@

$(OUT_CSS_DIR)/%.css: $(IN_CSS_DIR)/%.css
	@mkdir -p $(@D)
	cp $< $@

$(OUT_IMG_DIR)/%: $(IN_IMG_DIR)/%
	@mkdir -p $(@D)
	cp $< $@