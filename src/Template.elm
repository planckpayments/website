module Template exposing (Msg, Model, init, update, view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)

import HamburgerMenuIcon

type Msg = ToggleMenu
type Model = MenuOn | MenuOff

init : Model
init = MenuOff

update : Msg -> Model -> Model
update msg model = case msg of
  ToggleMenu -> case model of
    MenuOn -> MenuOff
    MenuOff -> MenuOn

view : (Msg -> msg) -> Model -> List (Html msg) -> List (Html msg)
view msgCtor model mainContent =
  let
    menuDisplay : String
    menuDisplay = case model of
      MenuOn -> "block"
      MenuOff -> "none"
  in
    [ Html.map msgCtor <| header []
      [ div [ class "centering" ]
        [ nav [ class "nav-layout wide-column" ]
          [ a [href "/", class "logo"] []
          , div [ class "widescreen-only"]
            [ a [ href "/#use-cases", class "sub-nav-link"] [text "CASES" ]
            , div [class "sub-nav-link-divider"] []
            , a [ href "/documents", class "sub-nav-link"] [ text "DOCS"]
            ]
          , div [class "smallscreen-only"]
            [ div [ class "burger-menu-switch", href "", onClick ToggleMenu ] [ HamburgerMenuIcon.view ]
            , ul [ class "burger-menu", style "display" menuDisplay ]
              [ li [] [a [ href "/"] [ text "Cases" ] ]
              , hr [] []
              , li [] [ a [href "/documents"] [text "Documents"]]
              , hr [] []
              ]
            ]
          ]
        ]
      , div [class "banner"] []
      ] 
    , div [] mainContent
    , footer [class "centering"]
      [ text "Contact: "
      , a [href "mailto:info@planckpayments.com"][text "info@planckpayments.com"]
      ]
    ]