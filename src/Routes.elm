module Routes exposing (..)

import Url exposing (Url)
import Url.Parser as P exposing (Parser, (</>))

type Route
  = Cases
  | Docs
  | Case CaseRoute
  | Err404

type CaseRoute
  = Micropayments
  | TollRoads


routeParser : Parser (Route -> a) a
routeParser = 
  let
    routeParser_case : Parser (CaseRoute -> b) b
    routeParser_case = P.oneOf
      [ P.map Micropayments <| P.s "micropayments"
      , P.map TollRoads <| P.s "toll-roads"
      ]

  in P.oneOf
    [ P.map Cases P.top
    , P.map Docs <| P.s "documents"
    , P.map Case <| P.s "cases" </> routeParser_case
    ]



urlToRoute : Url -> Route
urlToRoute url = Maybe.withDefault Err404 <| P.parse routeParser url

