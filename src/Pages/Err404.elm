module Pages.Err404 exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)

content : List (Html msg)
content =   
  [ div [ class "main-layout" ]
    [ div [] 
      [ div [class "text-column"]
        [ h1 [] [text "404"]
        , p [] [text "We could not find what you are looking for"]
        ]
      ]
    ]
  ]