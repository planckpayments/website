module Pages.Cases.Micropayments exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)

import Pages.Cases.CaseBase exposing (CasePreview, caseElem)

preview : CasePreview
preview =
  { title = "Browsing without ads or subscriptions"
  , summary = "Ads and subscriptions are a convoluted way to get money from consumers to creators. Micropayments are a known solution. We can deliver that."
  , imgSrc = "/imgs/case-vods/video-site-01.png"
  }
content : List (Html msg)
content = 
  [ div [ class "main-layout" ]
    [ div [] 
      [ div [class "wide-column"]
        [ caseElem
          "Did you know?"
           """The time you spend watching ads is worth less than minimum wage.
           Content creators on platforms like YouTube receive less than a cent from each advertisement you watch.
           """
           ""
        , caseElem 
          "The subscriptions problem"
          "Subscription services like Netflix seemed like a solution at first. Now, everybody is making their own. You need many subscriptions to watch every show you want. Many users are resorting to piracy."
          ""
        , caseElem
          "Just pay for it"
          "Paying content creators directly with a micropayment is the cheapest and most consumer-friendly solution. PlanckPayments enables micropayments through cheap and fast transactions."
          "/imgs/case-vods/video-site-01.png"
        ]
      ]
    , div []
      [ div [class "text-column needs-some-space"]
        [ h2 [] [text "Details"]
        , p [] 
          [ text "Micropayments, not ads, were intended to power the internet. Because existing payment systems could not offer micropayments, websites resorted to ads for monetization." ]
        , p []
          [ text "Traditional payment systems cannot cope with micropayments, or automation of payments in general. They hold the payer accountable for asynchronous errors. This puts the burdens of mental accounting and strong authentication on the payer." ]
        , p []
          [ text "That approach does not work with micropayment because it requires double-checking every payment request. Nobody wants to use double factor authentication for a payment of half a cent, nor do people want to go through a list of thousands of sub-cent payments to check if any of them are fraudulent."
          ]
        , p []
          [ text "PlanckPayments allows recipients to forgoe some payment security in exchange for a much better user experience. Some payments will be cancelled after the fact, but when throughput is high, volume matters more."]
        , p []
          [ text "Additionally, PlanckPayments is an open specification. Any PSP can offer PlanckPayments, and does not require complex contracts to do so. This is a much stronger basis to build a micropayments market than the ephemeral attempts of the past."]
        ]
      ]
    ]
  ]