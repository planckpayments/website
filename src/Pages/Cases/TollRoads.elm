module Pages.Cases.TollRoads exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)

import Pages.Cases.CaseBase exposing (CasePreview, caseElem)

preview : CasePreview
preview =
  { title = "Speed and privacy on toll roads"
  , summary = "Paid road usage without tracking or toll gates is possible. With PlanckPayments, payment happens automatically through your banking app while you are driving."
  , imgSrc = "/imgs/case-tollroads/app-preview.png"
  }

content : List (Html msg)
content = 
  [ div [ class "main-layout" ]
    [ div [] 
      [ div [class "wide-column"]
        [ caseElem 
          "Toll booths or tracking?"
          "Traditional payment system require strong authentication. That means either stopping at a toll booth to authenticate, or sign a contract that allows the toll agency to track your movements and charge your account accordingly."
          ""
        , caseElem
          "Toll booths"
          """The downsides of toll booths are obvious: long waiting queues. Telepass is better, but still requires slowing down to 30 km/h. Telepass also requires setting up a contract beforehand, which is more difficult for foreigners.
          
Toll booths also constitute an additional expense, and are a restriction on infrastructure planning.
          """
          "/imgs/stock/toll-queue.jpg"
        , caseElem
          "Tracking & privacy"
          "Most proposals for pay-by-use road tax require a device that tracks the driver. Citizens are rightly uncomfortable with letting governments know their every move."
          ""
        , caseElem
          "Our Solution"
          "Because PlanckPayments can be made by machines, a mobile phone app can make payments while you drive. No tracking is required. Your phone only broadcasts cryptographic proof of payment to toll collectors."
          "/imgs/case-tollroads/app-preview.png"
        ]
      ]
    ]
  ]