module Pages.Cases.CaseBase exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)

type alias CasePreview =
  { title : String
  , imgSrc : String
  , summary : String
  }

caseElem : String -> String -> String -> Html msg
caseElem title subtitle imgSrc = 
  case imgSrc of
    "" -> div [class "case-elem-alt"] 
      [ p [] 
        [ h2 [] [text title]
        , text subtitle
        ]
      ]
    _ ->
      div [ class "case-elem case-elem-textleft"]
        [ p [] 
          [ h2 [] [text title]
          , text subtitle
          ]
        , img [src imgSrc] []
        ]