module Pages.Docs exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)

content : List (Html msg)
content = 
  [ div [ class "main-layout" ]
    [ div [] 
      [ div [class "text-column"] 
        [ h1 [] [text "Documents"]
        , h4 []
          [ a [href "https://planckpayments.gitlab.io/documents/technical/whitepaper-spec/whitepaper-spec.pdf"][text "Whitepaper (early draft)"]
          ]
        , text "A high-level, technical presentation of the Planck Payments protocol. Motivated by references to earlier literature."
        , hr [] []
        , h4 [] [ a [href "https://planckpayments.gitlab.io/documents/technical/spec/spec.pdf"] [text "Specification (early draft)"] ]
        , text "A living document that fully specifies what rules must be followed by implementations of the Planck Payments protocol. This document is more detailed than the whitepaper but does not explore the technical motivation behind the protocol as fully."
        , hr [] []  
        , h4 [] [ a [href "https://button.demo.planckpayments.com/docs"] [text "Integration docs (demo, unstable API)"] ]
        , text "A demo to showcase how one-click payments can be integrated into a website using Planck Payments technology. No real payments are made. See a "
        , a [href "https://button.demo.planckpayments.com/example.html" ] [ text "minimal example" ]
        ]
      ]
    ]
  ]
