module Pages.About exposing (content)

import Html exposing (..)
import Html.Attributes exposing (..)

import Pages.Cases.CaseBase exposing (CasePreview)
import Pages.Cases.Micropayments as Micropayments
import Pages.Cases.TollRoads as TollRoads

content : List ( Html msg )
content = 
  [ div [ class "main-layout" ]
    [ div []
      [ div [ class "text-column"]
        [ h1 [] [text "Automated, web-scale payments" ]
        , p [] [ text "PlanckPayments is a bold new way to process payments. It allows for payments that are tiny, machine-driven, anonymous, and quick. All of that at a massive scale."] 
        ]
      ]
    , div [id "use-cases"]
      [ div [ class "wide-column"]
        [ h1 [] [text "Use cases"]
        , div [ class "cases-container"]
          [ casePreview Micropayments.preview <| Just "/cases/micropayments"
          , casePreview TollRoads.preview <| Just "/cases/toll-roads"
          , casePreview caseCloud Nothing
          , casePreview caseSocialClubs Nothing
          , casePreview caseSmartGrids Nothing
          , casePreview caseGaming Nothing
          ]
        ]
      ]
    ]
  ]

casePreview : CasePreview -> Maybe String ->  Html msg
casePreview casePrev mHref = 
  div [ class "case"]
    [ p [] 
      [ h2 [] [text casePrev.title]
      , text casePrev.summary
      , case mHref of
        Nothing -> text ""
        Just urlString -> 
          div [class "case-button-container"]
            [ a [href urlString] 
              [ div [ class "case-button"] 
                [ text "See case"
                ]
              ]
            ]
      ]
    , img [src casePrev.imgSrc] []
    ]

caseCloud : CasePreview
caseCloud =
  { title = "Cloud on demand"
  , imgSrc = "/imgs/stock/drives-rack.jpg"
  , summary = "Access cloud resources like compute, storage or machine learning without contract using pay-by-use."
  -- Note: include GPL distribution and renderfarms
  }

caseSocialClubs : CasePreview
caseSocialClubs =
  { title = "Low-barrier electronic payments"
  , imgSrc = ""
  , summary = "Pin machines are not viable for every location. Beaches, shared spaces, in transit... PlanckPayments requires only a mobile phone and an internet connection."
  }

caseSmartGrids : CasePreview
caseSmartGrids =
  { title = "Smart grids"
  , imgSrc = ""
  , summary = "Smart grids for renewable energy require IoT-ready payment services. PlanckPayments is just that."
  }

caseGaming : CasePreview
caseGaming =
  { title = "Gaming"
  , imgSrc = ""
  , summary = "Microtransactions are common in video games, but they have a high barrier to entry. High payment fees make buying a single item impossible."
  }