---
title: Micropayments are inevitable
small-body: true
---

# Micropayments are inevitable
#### 14 May 2021 by Oscar Leijendekker

### History
Feelings about micropayments have varied greatly over time. During the early days of the internet it was expected micropayments were a necessity and would be developed fairly quickly. Some attempts were made, and the W3C even created a standard for micropayments<sup>[[1]](#w3c-standard)</sup>, but as of today, no system has been widely adopted.

Later, as advertisement-based revenue gained popularity, many thought the micropayments problem was solved<sup>[[2]](#mercator-solving-micropayments)</sup>, sometimes stating that users would be unwilling to pay for content that was previously free. The popularity of services like Patreon, which allow consumers to voluntarily support content creators, has shown the latter belief to be incorrect. Meanwhile, resistance against advertising and its related technologies is growing, and new laws, such as GDPR, are being introduced to restrain the influence and effectiveness of advertisements.

Subscription-based services such as Netflix have also lead market leaders to believe pay-by-use and micropayments were dead, but the offer of subscription-based digital media has grown fragmented. Where once there was only Netflix, which had nearly everything users wanted, now there are many competing services with different offerings. The dissatisfaction with the current situation can be observed in the return of illegal media downloading<sup>[[3]](#sandvine-piracy)</sup>.

Advertisements and subscriptions have run ahead of micropayments not because they are better business models, but because they are simpler, both from a technological and organizational perspective. Fundamentally, neither actually solves the problem of selling digital content: getting a fair amount of money from consumers to producers in exchange for access to content.

### Advertisements

In the case of advertisements, money instead goes from advertisers to producers, and in exchange, advertisers are allowed to influence consumers. This completely changes the business model. Advertisers become the clients and viewers become the product. Catering to advertisers becomes the new priority, with potentially damaging long-term effects<sup>[[4]](#gwern-ads).

Advertisers further expect a positive return on investment, easily as much as 8 times. This means that, on average, a viewer pays 8 times what the creator receives. In any regular payment system, transaction costs of 800% would be absolutely prohibitive. While some users may think they are not influenced by advertisements, this is incorrect, as advertisers have advanced machine learning models to ensure their advertisements are effective. If a user were to truly be immune to the influence of advertisements, content creators who depend on ad income would eventually simply not cater to that user. Even if we ignore ROI, watching advertisements would still consist of working below minimum wage if we consider the recompense that goes to the content creator for a single advert.

There are further problems of indeterminate value, like the loss of privacy and the negative influence of advertisement-related strategies on society, that are creating increasing resistance against ads.

### Subscriptions

On the other hand, subscriptions have the essential problem that they do not allow users to purchase the content they want. Instead, they usually provide time-limited access to some preselected, ever-changing collection. Fixed-rate payments work fine, and are even preferred, for services such as phone costs, news, and utilities, but not for incomplete content collections. A newspaper is expected to cover all stories of import, and phones are expected to operate at all times. Incompleteness of a subscription service is not accepted, regardless of how much *is* included. Neither should we expect consumers to be happy to pay a monthly fee for access to a fraction of the content they want access to. To prevent fragmentation of the offering, subscription-based content platforms need to be monopolies, but that is not a desirable situation either.

Subscriptions may work well for content creators that are well-loved by their fan base. Patreon is a prime example for this. Even in this case, however, it forces creators to focus on frequency and consistency of releases, rather than quality of individual pieces. It's doubtful that the model works with larger, more anonymous organizations, and it's unclear if it would still be competitive if micropayments existed in a meaningful capacity.


### In conclusion

The internet constitutes a great platform for mass distribution of digital content, but lacks an appropriate accompanying monetization scheme. Advertisements and subscriptions may have seemed to fill that void for a while, but we are becoming increasingly aware of their drawbacks. Any scheme that complicates the proposition of simple transactions (money in exchange for content consumption) puts unnecessary complexity onto the business model with increased potential points of failure. A micropayment system will therefore always be the most efficient system for content sale. I must note here, that not everything t

The apparent lack of demand for micropayments is simply a matter of survivorship bias: business models that would depend on micropayments cannot exist without them.



## References
- <a name="w3c-standard">[1]</a> [Micropayments Overview](https://www.w3.org/ECommerce/Micropayments/Overview.html). W3C. Retrieved 11-5-2021.
- <a name="mercator-solving-micropayments">[2]</a> [
Solving the Micropayment Problem: Online and at the POS](https://www.mercatoradvisorygroup.com/Reports/Solving-the-Micropayment-Problem---Online-and-at-the-POS/). Mercator Research. 30-04-2007. Retrieved 11-5-2021.
- <a name="sandvine-piracy">[3]</a> [Global Internet Phenomena Preview: File sharing on the internet reverses a downward trend](https://www.sandvine.com/blog/global-internet-phenomena-preview-file-sharing-reverses-a-downward-trend). Cam Cullen, Sandvine. 24-09-2018. Retrieved 10-5-2021.
- <a name="gwern-ads">[4]</a> [Banner Ads Considered Harmful](https://www.gwern.net/Ads). Gwern. Ongoing. Retrieved 12-5-2021
