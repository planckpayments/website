---
title: The role of micro payments in IoT
small-body: true
date: 9 Oct 2020
---

# The role of micro payments in IoT
##### 9 October 2020 by Oscar Leijendekker

The rise of IoT has reinvigorated interest in micro payments. The premise of
machines interacting with each other opens up the possibility of making payments
that would be too small or too frequent for humans. IOTA estimates a future
yearly payment stream of 700 billion dollars for IoT payments, but a lack of
projects using the technology and a low coin value indicates some
skepticism from the market. So how relevant are micro payments for IoT?

The market for IoT, in so far as it isn't a re-brand of embedded systems,
is still being explored, and making any claims as to it's final size, especially
when it comes to the segment with potential for micro payments, would be
mere speculation. We can, however, look at the technical side of things to
determine which types of products can and cannot benefit from micro payments.

In many cases where one might want to employ pay-by-use strategies it is
sufficient to meter usage and invoice the total costs monthly. This does require
some form of trust between payer and payee, such as a contract or setting up
some other form of payment plan. But even though this constitutes a potentially
undesired barrier to entry, any application in which devices are physically
placed with the payer already requires establishing such trust, as the user
might steal or damage the machine.
Proposals such as pay-by-use washing machines are thus not going to benefit
from micro payments.

An exception to this is when privacy is a selling point. A device does not need
to know who made a payment to function, so long as it knows a payment is made.
This would allow pay-by-use without requiring the storage of usage statistics.
Smart grids are a good example of such an application; devices with postponable
workloads could buy electricity when it is cheap, but users may not want to
divulge their electricity usage, and indeed objections to smart meters have
already arisen.

A more common use-case can be found in transitional spaces, such as
stations, airports, roads, publicly accessible buildings... places where many
people pass every day. There it is much harder to establish contracts
with potential users, and many may pass too infrequently to set up
a traditional invoicing scheme. This is where micro payments can really shine.
We already have examples of similar devices in the form of vending machines,
paid public toilets, and massage chairs in airports.
Micro payments could further expand the scope to
cheaper or more fine-grained services such as wireless internet connections,
induction charging roads, or road taxation by kilometer.

Finally, micro payments can come in handy when sharing devices ad-hoc. Devices
that could benefit from being shared include drones for making quick small
deliveries, 3D printers for one-off prints that don't warrant buying an entire
printer, laser cutters, and powerful compute clusters. While this may remain
a p2p dream for artists and hackers with slim margins, the potential of
pay-by-use in communities of users is certainly there.
