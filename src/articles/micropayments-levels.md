---
title: The three levels of micro payments
small-body: true
date: 14 Oct 2020
---

# The three levels of micro payments
##### 14 October 2020 by Oscar Leijendekker

Micro payments have been used in various form for quite a while. We have
categorize them in 3 abstract designs of increasing complexity and explain
the pros and cons of each.



## Many-to-one


![many-to-one](/imgs/micropayments-levels/lvl1.png)

The simplest - and currently most widely used - way by which to make micro
payments is by establishing a trusted payment channel and adopting fine-grained
metering. Prime examples are prepaid phone charges, electricity bills and
micro-transactions in video games.

This approach has some definite advantages. The vendor is completely independent
from third-party micro payment processors. From a technical perspective,
metering is also a very simple operation to scale because it relies on addition,
which is associative and commutative, allowing the usage of reduce operations
without worry about race-condition. Data loss also becomes easy to deal with:
if the vendor loses usage data, he can simply not charge the buyer for that
"forgotten" usage.

The problems with metering are barrier-to-entry and privacy concerns.

It is not interesting for buyers to establish a payment channel for one-off
purchases, reducing the vendors ability to capitalize one specific items. This
is especially relevant in transitional spaces where many potential clients may
pass, but not stay long enough to establish a payment channel. It may also be
difficult for newcomers to penetrate an existing market using payment channels.

Metering also requires trusting the vendor's handling of buyer's usage data.
We're already seeing push-back against e.g. smart electrical meters.


## Many-to-one-to-many

![many-to-one-to-many](/imgs/micropayments-levels/lvl2.png)

Most micro payment proposals envision a single processor that sets up payment
channels with buyers and sellers, similar to the 3-corner model in credit cards.

This strategy does not offer many advantages in practice. Barrier-to-entry would
be reduced compared to a many-to-one approach if a sufficient amount of buyers
and vendors setup payment channels with the same processors. However, that means
the proposition is only interesting to vendors once it is already popular,
creating a circular dependency.

Privacy is not improved either. While usage data can be hidden from the vendor,
many-to-one-to-many proposals strongly rely on a monopolistic position of the
payment processor, denying any realistic expectation of privacy from the
processor due to a simple lack of competition.

The large body of failed micro payment attempts that fall into this category
is perhaps the best indicator that many-to-one-to-many approaches are a fool's
errand.


## Many-to-many-to-many

![many-to-many-to-many](/imgs/micropayments-levels/lvl3.png)

To resolve the issues of a many-to-one-to-many system, we are creating the
Planck Payments protocol. This is a specification through which adhering micro
payment processors could effectuate payments to any vendor that accepts
them.

This strategy removes the need to rely on a single processor and its potential
monopoly, further reducing barriers-to-entry in practice, as buyers connected
to one adhering processor would be able to pay to any connected vendor, not just
the ones with whom their processor has established contracts. In essence, the
system aims to expand the reach of micro payments by connecting all vendors to
all processors.

Privacy can thus also be achieved as it may become a selling point for competing
micro payment processors.

The biggest issue of a many-to-many-to-many system is how to establish trust.
Traditional approaches will not work, as micro payments will necessarily have
processing errors as a result of the poor scaling of payments. More complicated
systems are required for this purpose. In our case we rely on cryptography and
non-binary trust scores.


## But what about crypto?

We have only covered systems based on aggregation, and not e.g.
cryptocurrencies, also called virtual currencies, where transactions are made
directly on a clever distributed datastructure. Virtual currencies may be able
to offer low transaction costs, but that is not an essential problem of micro
payments. Every single failed micro payment approach managed to lower
transaction costs.

The problems with the use of virtual currencies for micro payments merit an
article of their own, but we can summarize them by stating that micro payments
require trust in order to deal with necessary processing errors, but trust is
the very thing virtual currencies seek to avoid.
