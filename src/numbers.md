---
small-body: true
title: Planck Payments
---

# The numbers

Realizing micro payments in any useful capacity requires absolutely ridiculous
scalability.

Let's assume, for example, that YouTube would like to switch to micro payments. YouTube has about 4 billion video views per day.<sup>[[1]](https://www.jeffbullas.com/35-mind-numbing-youtube-facts-figures-and-statistics-infographic/)</sup> That means we'd have to process over 46k transactions every second.
In comparison, Visa processes less than 2k transactions each second. It
claims to be able to handle 56k transactions in theory, which would
be enough for just YouTube, provided there are no peaks.
Things get worse for the pay-per-second model, in which case we need to process
4 million transactions every second, based on the 3 billion hours of video
watched each month.
