module Main exposing (main)

import Browser exposing (UrlRequest(..))
import Browser.Navigation exposing (Key)
import Browser.Dom exposing (setViewport, getElement)
import Url exposing (Url)
import Html exposing (..)
import Html.Attributes exposing (..)
import Task

import Routes exposing (Route(..), CaseRoute(..), urlToRoute)
import Template
import Pages.About
import Pages.Docs
import Pages.Cases.Micropayments
import Pages.Cases.TollRoads
import Pages.Err404



type alias Flags = ()
type Msg 
  = OnUrlRequest UrlRequest
  | OnUrlChange Url
  | TemplateMsg Template.Msg
  | NoOp

type alias Model =
  { navKey : Key
  , currentRoute : Route
  , templateModel : Template.Model
  }


main : Program Flags Model Msg
main = Browser.application 
  { init = init
  , update = update
  , subscriptions = subscriptions
  , view = view
  , onUrlRequest = OnUrlRequest
  , onUrlChange = OnUrlChange
  }


init : Flags -> Url -> Key -> ( Model, Cmd Msg )
init _ url key = 
    ( { navKey = key
      , currentRoute = urlToRoute url
      , templateModel = Template.init
      }
    , Cmd.none
    )

scrollTo : Maybe String -> Task.Task x ()
scrollTo fragment = 
  ( case fragment of
    Nothing -> Task.succeed (0, 0)
    Just elementId -> getElement elementId 
      |> Task.map (\element -> (element.element.x, element.element.y))
      |> Task.onError (\_ -> Task.succeed (0, 0))
  ) |> Task.andThen (\(x, y) -> setViewport x y)

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model = case msg of
  NoOp -> ( model, Cmd.none )
  OnUrlRequest urlRequest -> 
    case urlRequest of
      Internal url -> (model, Browser.Navigation.pushUrl model.navKey <| Url.toString url)
      External urlString -> 
        (model, Browser.Navigation.load urlString)
  OnUrlChange url -> 
    ( { model | currentRoute = urlToRoute url, templateModel = Template.init }
    , Task.perform (\_ -> NoOp) <| scrollTo url.fragment)
  TemplateMsg innerMsg ->
    ( { model | templateModel = Template.update innerMsg model.templateModel}, Cmd.none )

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none


view : Model -> Browser.Document Msg
view model = 
  { title = "PlanckPayments"
  , body = Template.view TemplateMsg model.templateModel <| case model.currentRoute of
    Cases -> Pages.About.content
    Docs -> Pages.Docs.content
    Case c -> case c of
      Micropayments -> Pages.Cases.Micropayments.content
      TollRoads -> Pages.Cases.TollRoads.content
    Err404 -> Pages.Err404.content
  }